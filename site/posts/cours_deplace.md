---
title: Labo du 14 remplacé par un cours
date: '2013-01-08'
description:
categories:
---

La séance de laboratoire prévu pour lundi sera exceptionnellement remplacée par un cours pour nous donner le temps de couvrir la matière essentielle. Cette séance sera reprise le mercredi matin au local A-3332. Consultez le [calendrier](calendrier) pour les détails.
