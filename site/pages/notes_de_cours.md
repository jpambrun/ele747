---
title : Notes de cours
description:
---

Le code source des présentations du cours est disponible sur [bitbucket.org](https://bitbucket.org/jpambrun/ele747/).  
Les erreurs de français, les coquilles et les passages ambigus peuvent être rapportés dans le [issue tracker](https://bitbucket.org/jpambrun/ele747/issues).

Cours 1
---------------------
Introduction, Révision, Système visuel humain et Acquisition  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours01_Presentation.pdf), [Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours01_Handout.pdf), Code Matlab

Cours 2
---------------------
Relation entre les pixels, Opérations de base et Histogramme  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours02_Presentation.pdf), [Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours02_Handout.pdf), [Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours02_matlab.zip)

Cours 3
---------------------
Transformations géométriques, Filtrage spatial et Filtres passe-bas  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours03_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours03_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours03_matlab.m)

Cours 4
---------------------
Filtres passe-haut  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours04_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours04_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours04_matlab.m)

Cours 5
---------------------
Segmentation et détection de contours  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours05_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours05_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours05_matlab.zip)

Cours 6
---------------------
Opérations morphologiques  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours06_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours06_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours06_matlab.zip)

Cours 7
---------------------
Couleur  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours07_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours07_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours07_matlab.zip)

Cours 10
---------------------
Transformée de Fourier et filtrage fréquentiel  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours10_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours10_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours10_matlab.zip)

Cours 11
---------------------
Compression d'images  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours11_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours11_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours11_matlab.zip)

Cours 12
---------------------
Compression avec JPEG et JPEG2000  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours12_Presentation.pdf), 
[Handout](https://bitbucket.org/jpambrun/ele747/downloads/cours12_Handout.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours12_matlab.zip)

Cours 13
---------------------
Introduction à la vision par ordinateur  
[Présentation](https://bitbucket.org/jpambrun/ele747/downloads/cours13_Presentation.pdf), 
[Code Matlab](https://bitbucket.org/jpambrun/ele747/downloads/cours13_matlab.zip)
