---
title : Laboratoires
description: Labo
---
Les dates de remises sont inscrites dans le [calendrier](calendrier). Ne soyez pas en retard!!


## Laboratoire 1 : Opérations de base  
Pondération : 5.83% (2/12*35)  
Temps : 2 séances (4h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo1.pdf), 
[Fichers requis](https://bitbucket.org/jpambrun/ele747/downloads/images_labs.zip)

## Laboratoire 2 : Détection de contour  
Pondération : 11.66% (4/12*35)
Temps : 4 séances (8h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo2.pdf), 
[Fichers requis](https://bitbucket.org/jpambrun/ele747/downloads/images_lab2.zip)

## Laboratoire 3 : Morphologie mathématique
Pondération : 2.91% (1/12*35)
Temps : 1 séance (2h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo3.pdf), 
[Fichers requis](https://bitbucket.org/jpambrun/ele747/downloads/images_lab3.zip)


## Laboratoire 4 : Couleur
Pondération : 5.83% (2/12*35)  
Temps : 2 séances (4h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo4.pdf), 
[Fichers requis](https://bitbucket.org/jpambrun/ele747/downloads/images_lab4.zip)

## Laboratoire 5 : Filtrage dans le domaine fréquentiel
Pondération : 5.83% (2/12*35)  
Temps : 2 séances (4h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo5.pdf) 

## Laboratoire 6 : Compression
Pondération : 2.91% (1/12*35)
Temps : 1 séance (2h)  
[Énoncé](https://bitbucket.org/jpambrun/ele747/downloads/labo6.pdf) 
