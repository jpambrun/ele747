---
title : Chargés et professeure
description:
---

Chargé de cours
---------------
> Pambrun, Jean-François  
> Local : A-2442  
> Courriel : <jf.pambrun@gmail.com>


Chargé de Laboratoires
---------------
> Beaudry, Julien  
> Local : A-2442  
> Courriel : <julien.beaudry.1@ens.etsmtl.ca>


Professeure Responsable
---------------
> Noumeir, Rita   
> Local : A-2466  
> Téléphone : (514) 396-8800, poste 8671   
> Courriel : <rita.noumeir@etsmtl.ca>


 
