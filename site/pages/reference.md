---
title : Références
description:
---


Manuels de référence
====================

### Digital Image Processing (Obligatoire)
#### by Rafael C. Gonzalez and Richard E. Woods   
</br>     


### Digital Image Processing Using Matlab (Complémentaire)
#### by Rafael C. Gonzalez, Richard E. Woods and Steven L. Eddins
</br> 

### Fundamentals of Electronic Image Processing (Complémentaire)
#### by Arthur R. Weeks
</br> 

### Introduction au traitement d'images (Complémentaire)
#### by Diane Lingrand

</br>  	

Site web
====================
[YAGTOM: Yet Another Guide TO Matlab](https://code.google.com/p/yagtom/)