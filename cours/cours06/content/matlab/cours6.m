%% erosion
A=double(imread('wirebond_mask.png'));
B=strel('disk',14);
A2=imerode(A,B);
A3=imdilate(A2,B);
imshow([A A2 A3]);
xlabel('a) Image originale, b) erosion c) dilatation de b ')

%% dilatation
A=double(imread('broken_text.png'));
B=[0 1 0; 1 1 1; 0 1 0];
A2=imdilate(A,B);
imshow([A A2]);
xlabel('a) Image originale, b) dilatation')

%% opening 
f = double(imread('shapes.png'));
se = strel('square',20);

% affichage de se au corrdonne 15,15 avec la dilatation
el = zeros(size(f));
el(15,15)=1;
el = imdilate(el,se);

% ouverture a bras
fe  = imerode(f,se);
fed = imdilate(fe,se);
imshow([f+el*.5; fe; fed])
xlabel('a) Image originale et SE (gris), b) erosion, c) dilatation')

% ouverture matlab
fo = imopen(f,se);
figure;imshow([f; fo])
xlabel('a) Image originale , b) imopen')
%% closing
f = double(imread('shapes.png'));
se = strel('square',20);
el = zeros(size(f));el(15,15)=1;el = imdilate(el,se);

fd  = imdilate(f,se);
fde = imerode(fd,se);
imshow([f+el*.5; fd; fde])
xlabel('a) Image originale et SE (gris), b) dilatation, c) erosion')

fc = imclose(f,se);
figure;imshow([f+el*.5; fc;])
xlabel('a) Image originale , b) imclose')

%% opening ET closing
f = double(imread('shapes.png'));
se = strel('square',20);
fo = imopen(f,se);
foc = imclose(fo,se);

imshow([f; fo; foc;])
xlabel('a) Image originale , b) imopen, c) imclose')
%% open ET close - application denoising
f = double(imread('noisy_fingerprint.png'));
se = strel('square',3);
fo = imopen(f,se);
foc = imclose(fo,se);

imshow([f fo foc  f-foc])
xlabel('a) Image originale, b) open, c) close, d) bruit retiré')
%% tout ou rien
f = zeros(20,20);
f(5,5)=1;                       % 1 pixel seul
f(5,10)=1;f(5,11)=1;            % 2 pixels ( -  )
f(15,15)=1;f(16,15)=1;          % 2 pixels ( |  )
f(10,5)=1;f(10,6)=1;f(11,5)=1;  % 3 pixel  ( |_ )

A=[0 0 0;0 1 1; 0 1 0];
B=[1 1 1;1 0 0; 1 0 1];
 % A=[0 0 0;0 1 1; 0 0 0];
bw = bwhitmiss(f,A,B);
imshow([f bw], 'InitialMagnification', 3000)
xlabel('a) Image originale, b) Hit or Miss' )

%% hole filling
f = imcomplement(logical(imread('ten_objects.png')));
f2=imfill(f,[220,125]);
imshow([f f2]);
xlabel('a) Image originale, b) Hole filled' )

%% thinning
f = double(imread('ten_objects.png'));
f2 = bwmorph(f,'thin',Inf);
imshow([f f2]);
xlabel('a) Image originale, b) thinned' )

%% thickening
f3 = bwmorph(f2,'thicken',2);
imshow([f f2 f3]);
xlabel('a) Image originale, b) thinned' )


%% gray scale
f = imread('aerial.png');
se = strel('square',3);
gd = imdilate(f,se);
ge = imerode(f,se);
mgrag = gd-ge;
imshow([[f mgrag];[gd ge]]);
xlabel('a) Image originale, b) gradient morpho, c) dilatation, d) erosion ' )



