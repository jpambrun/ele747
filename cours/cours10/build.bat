for %%* in (.) do set currentDir=%%~n*
texify -p Presentation.tex --tex-option=--jobname=%currentDir%_Presentation
texify -p PresentationOverlay.tex --tex-option=--jobname=%currentDir%_PresentationOverlay
texify -p Handout.tex --tex-option=--jobname=%currentDir%_Handout
move *.pdf ..\published
del *.aux *.fdb_latexmk *.log *.gz *.snm *.out *.nav *.toc