%% 1D fft
T=1/1000;t=0:T:1;
y = 0.7*sin(2*pi*50*t) + sin(2*pi*100*t);
% uncomment pour ajouter le bruit
%y = y + 0.3*randn(size(t));

subplot(2,1,1);plot(1/T*t(1:200),y(1:200));

Y = fft(y,1024);%Y complexe
f = (1/T)*linspace(0,1,1024);%linspace 0->1 en 1024 step
subplot(2,1,2);plot(f,abs(Y));


%% a bras vs matlab
T=1/1000;t=0:T:1;
y = 0.7*sin(2*pi*50*t) + sin(2*pi*100*t); 

% == a bras ==
M=size(y,2);x=0:M-1;
tic;
for u=0:M-1
    F(u+1) = 1/M *sum(y(x+1).*exp(i*2*pi*u*x/M));
end
toc
f = (1/T)*linspace(0,1,1001);
subplot(2,1,1);plot(f,abs(F));

% == matlab ==
tic;
Y = fft(y,1001);
toc;
subplot(2,1,2);plot(f,abs(Y/1001));

%% 2D fft
I=im2double(imread('cameraman.tif'));
F = fft(fft(I).').'; %separabilite XY
imshow(abs(F),[]);
%imshow(log(1 + abs(F)),[]);


%% d�calage 1D / shifting (-1)^x
T=1/1000;t=0:T:1;
y = 0.7*sin(2*pi*50*t) + sin(2*pi*100*t); 

f = (1/T)*linspace(0,1,1024);

Y = fft(y,1024);
Y1 = fft(y.*(-1).^(1:1001) ,1024); %Translation dans DF

subplot(2,1,1);plot(f,abs(Y));
subplot(2,1,2);plot(f,abs(Y1));

%% d�calage 2D 
I=im2double(imread('cameraman.tif'));
F = fft(fft(I).').'; %separabilite XY
Fc=fftshift(F);% == f(x,y) * (-1)^(x+y)
imshow([log(1 + abs(F))  log(1 + abs(Fc))],[]);

%% F(0,0)
I=im2double(imread('cameraman.tif'));
F = fft(fft(I).').'; %separabilite XY
Fc1=Fc;Fc1( floor(size(I,1)/2)+1 , floor(size(I,2)/2)+1 ) = 0;%F(0,0)=0
F1=ifftshift(Fc1);
I1=ifft2(F1);
imshow([I real(I1)]);%I1 a une composante imginaire a cause de round errors -> real(I1)

%% convoltion
I=im2double(imread('cameraman.tif'));
H=ones(5,5)/(5^2);
%freqz2(H);
Hf = freqz2(H, size(I));
%imagesc(Hf);
F = fft2(I);
Fc=fftshift(F);

Fc1=Fc.*Hf;

F1=ifftshift(Fc1);
I1=ifft2(F1);
imshow([I real(I1)]);

%% padding
I=im2double(imread('cameraman.tif'));
H=ones(5,5)/(5^2);
%tic;
F = fft2(I,  2*size(I,1),  2*size(I,2)); %idealement power2
%toc
%tic;
%F = fft2(I,  511,  511);
%toc;
%tic;
%F = fft2(I,  510,  510);
%toc;
Hf = freqz2(H, size(F));

Fc=fftshift(F);
Fc1=Fc.*Hf;
F1=ifftshift(Fc1);
I1=ifft2(F1);
imshow([I real(I1(1:size(I,1), 1:size(I,2)))]);

%% I[L,H]PF (encore sans padding)
I=im2double(imread('cameraman.tif'));

%Calculer D
[M,N]=size(I);
[x,y]= meshgrid(-M/2:(M-1)/2, -N/2:(N-1)/2);
D=sqrt(x.^2+y.^2);

Hf=false(M,N);Hf(D<40)=true;
%imshow(Hf);

F = fft2(I);
Fc=fftshift(F);

%lowpass
Fc1=Fc.*Hf;
F1=ifftshift(Fc1);
I1=ifft2(F1);

%highpass
Fc2=Fc.*~Hf;% ~ == 1-Hf
F2=ifftshift(Fc2);
I2=ifft2(F2);

imshow([I real(I1) real(I2) ]);

%mesh(double(Hf));

%% Sobel (sans padding oups..)
I=im2double(imread('cameraman.tif'));
H=[-1 0 1;-2 0 2;-1 0 1];
%freqz2(H);
Hf = freqz2(H, size(I));

F = fft2(I);
Fc=fftshift(F);
Fc1=Fc.*Hf;
F1=ifftshift(Fc1);
I1=ifft2(F1);

imshow([I real(I1) filter2(H,I,'same')]);