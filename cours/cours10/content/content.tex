% !TEX root = ../Presentation.tex

\title{ELE747 - COURS 10 \\ Traitement dans le \\domaine de Fourier~}
\author{Jean-François Pambrun\\ {\it École de technologie supérieure}\\}
\date{Hiver 2013}
\usepackage{verbatim}
\begin{document}
\begin{frame}[plain]\mytitlepage\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Intro \& Rappel}
\subsection{Introduction}
\begin{frame}{\insertsubsection}
  Plan de la séance
  \begin{itemize}
  \item Rappel
    \begin{itemize}
    \item Nombres complexes
    \item Séries de Fourier
    \item Impulsion et train d'impulsions
    \item Transformée de Fourier
    \end{itemize}
  \item Convolution dans le domaine Fourier
  \item Théorème d'échantillonnage
  \item Transformée de Fourier Discrète (DFT)
  \item Filtrage dans le domaine de Fourier
    \begin{itemize}
    \item Low-pass
    \item High-pass
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{itemize}
  \item En 1822, Jean Baptiste Joseph Fourier introduit les séries de Fourier dans son livre \og La Théorie Analytique de la Chaleur\fg{}
  \item Elles permettent de représenter toute fonction périodique par une somme infinie de sinus et de cosinus de différentes fréquences pondéré par différents coefficients
  \item Elles permettent également de représenter une fonction non périodique et paire dont l'aire sous la courbe, l'intégrale, est finie.
  \end{itemize}
\end{frame}

\subsection{Nombres complexes}
\begin{frame}{\insertsubsection}
  Un \textbf{nombre complexe}, $C$, est représenté par:
  \[ \boxed{C=R+jI} \]
  où $R$ et $I$ sont des nombres réels et $j=\sqrt{-1}$.\\
  $R$ et $I$ représente respectivement les parties réelle et imaginaire.
  Le conjugué $C^*$ est noté $C^*=R-jI$.
  $C$ peut également être présenté en coordonnée polaire :
  \[ C=|C|(\cos(\theta)+j\sin(\theta)) \]
  où $|C|=\sqrt{R^2+I^2}$.\\
  En utilisant la formule d'Euler, $ e^{j\theta}=\cos(\theta)+j\sin(\theta) $, $C$ deviens :
  \[ \boxed{C=|C|e^{j\theta}} \]
\end{frame}

\subsection{Séries de Fourier}
\begin{frame}{\insertsubsection}
  Une fonction continue $f(t)$ périodique de période $T$ peut être représentée par une série infinie de sinus et de cosinus. Avec la formule d'Euler la \textbf{série de Fourier} prends la forme suivante :
  \[\boxed{f(t) = \sum\limits_{n =  - \infty }^\infty  {{c_n}{e^{j\frac{{2\pi n}}{T}t}}}  \hspace{.5cm}  {c_n} = \frac{1}{T}\int_{ - T/2}^{T/2} {f(t){e^{ - j\frac{{2\pi n}}{T}t}}dt} }\]
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/fseries.png}
    \caption{Reconstruction avec a) 20, b) 50, c)250 termes}
  \end{figure}
\end{frame}


\subsection{Impulsion unitaire}
\begin{frame}{\insertsubsection}
  Une \textbf{impulsion unitaire (Dirac)}, d'une variable continue $t$, est notée $\delta (t)$ et est définie :
  \[\delta (t) = \left\{ {\begin{array}{*{20}{c}} \infty &{{\text{if }}t = 0} \\ 0&{{\text{if }}t \ne 0} \end{array}} \right. \hspace{.5cm}   \int_{ - \infty }^\infty  {\delta (t)dt}  = 1\]
  Le Dirac est une pointe de longueur infinie d'une durée zéro (en considérant que $t$ représente le temps) ayant une aire sous la courbe de 1.
  Le Dirac a une \textbf{propriété de triage \og sifting \fg{} }:
  \[\int_{ - \infty }^\infty  {f(t)\delta (t)dt = f(0)} \]
  qu'on peut exploiter à un point arbitraire ($t_0$):
  \[\boxed{\int_{ - \infty }^\infty  {f(t)\delta (t - {t_0})dt = f({t_0})} }\]
\end{frame}

\begin{frame}{\insertsubsection}
  Si on considère $x$, une \textbf{variable discrète}, le Dirac discret, $\delta (x)$, devient :
  \[\delta (t) = \left\{ {\begin{array}{*{20}{c}} 1&{{\text{if x}} = 0} \\ 0&{{\text{if x}} \ne 0} \end{array}} \right. \hspace{.5cm} \sum\limits_{x =  - \infty }^\infty  {\delta (x)}  = 1 \]
  La propriété de triage, \og sifting \fg{}, devient donc :
  \[\sum\limits_{x =  - \infty }^\infty  {f(x)\delta (x)}  = f(0)\]
  qu'on peut déplacer a $x_0$:
  \[\boxed{\sum\limits_{x =  - \infty }^\infty  {f(x)\delta (x - {x_0})}  = f({x_0})}\]
\end{frame}

\subsection{Train d'impulsions}
\begin{frame}{\insertsubsection}
  On s'intéresse particulièrement au \textbf{train d'impulsions} défini comme une somme infinie de Dirac séparé par $\Delta T$ :
  \[{S_{\Delta T}}(t) = \sum\limits_{n =  - \infty }^\infty  {\delta (t - n\Delta T)} \]
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image5.png}
    \caption{Train d'impulsions (DIP)}
  \end{figure}
\end{frame}

\subsection{Transformée de Fourier}
\begin{frame}{\insertsubsection}
  La \textbf{transformée de Fourier} d'une fonction continue $f(t)$ notée $\Im \left\{ {f(t)} \right\}$ est défini par l'équation :
  \[\boxed{F(\mu) = \Im \left\{ {f(t)} \right\} = \int_{ - \infty }^\infty  {f(t){e^{ - j2\pi \mu t}}dt} }\]
  où $\mu$ est une variable continue (souvent noté $\omega$ en élé).
  $t$ est exclu par l'intégration, $\Im \left\{ {f(t)} \right\}$ dépend donc seulement de $\mu$
  Sachant $F(\mu)$, il est possible de retrouver $f(t)$ avec la \textbf{transformée de Fourier inverse} :
  \[\boxed{f(t) = \int_{ - \infty }^\infty  {F(\mu ){e^{j2\pi \mu t}}d\mu } } \]
  Notes : En général, la transformée est \textbf{complexe}. $\mu$ est une fréquence. Lorsque  $t$ représente le temps, $\mu$ est en cycles/sec. Lorsque $t$ représente une distance en mètre, $\mu$ correspond a des cycles/mètre.
\end{frame}

\begin{frame}{\insertsubsection}
  Exemple de transformée de Fourier avec une porte de largeur $W$ et d'amplitude $A$.
  Dans ce cas, $F(\mu)=AW{\rm sinc}(\mu W)$
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm]{content/figures/image7.png}
    \caption{Transformée de Fourier d'une porte (box) (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  Notons deux résultats importants:
  \begin{enumerate}
  \item La transformée de Fourier du train d'impulsion ${S_{\Delta T}}(t)$ est:
    \[S(\mu) = \Im \left\{ {{S_{\Delta T}}(t)} \right\} = \frac{1}{{\Delta T}}\sum\limits_{n = - \infty }^\infty {\delta \left( {\mu - \frac{n}{{\Delta T}}} \right)} \]
  \item La convolution de $f(t)$ avec $h(t)$ noté $f(t) \star h(t)$ :
    \[\int_{ - \infty }^\infty  {f(\tau )h(t - \tau )d\tau } \]
    est égal au produit dans le domaine de Fourier :
    \[\Im \left\{ {\int_{ - \infty }^\infty  {f(\tau )h(t - \tau )d\tau } } \right\} = H(\mu )F(\mu )\]
    et l'inverse est également vrai :
    \[\boxed{f(t) \star h(t) \Leftrightarrow H(\mu )F(\mu ) \hspace{.5cm}H(\mu ) \star F(\mu ) \Leftrightarrow f(t)h(t) }\]
  \end{enumerate}
\end{frame}

\subsection{Échantillonnage}
\begin{frame}{\insertsubsection}
  Les signaux continus doivent être convertis en signaux discrets (échantillonnage et quantification) avant d'être traités par un appareil numérique.
  \\
  L'\textbf{échantillonnage} consiste à approximer un signal continue avec un ensemble de valeurs ponctuelles séparé d'un intervalle uniforme $\Delta T$.
  Ceci peut être modélisé par le produit de $f(t)$ par un \emph{train d'impulsion}:
  \[\tilde f(t) = f(t) \cdot {S_{\Delta T}}(t) = \sum\limits_{x =  - \infty }^\infty  {f(t)\delta (t - n\Delta T)} \]
  Avec la propriété de sifting, la valeur $f_n$ du $n$-ième échantillon est donnée par $f(n\Delta T)$.
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=5cm]{content/figures/image8.png}
    \caption{Échantillonage a) $f(t)$, b) $S_{\Delta T}(t)$, c) produit de (a) et (b), d) échantillons   (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  La transformée de Fourier, $\tilde F(\mu )$, devient donc:
  \[\tilde F(\mu ) = \Im \left\{ {\tilde f(t)} \right\} = \Im \left\{ {f(t) \cdot {S_{\Delta T}}(t)} \right\} = F(\mu ) \star S(\mu )\]
  \[= \int_{ - \infty }^\infty {F(\tau )S(t - \tau )d\tau }\]
  %\[\tilde F(\mu ) = F(\mu ) \star \mathcal{S}(\mu ) = \int_{ - \infty }^\infty {F(\tau )S(t - \tau )d\tau } \]
  Après substitution de $S(\mu ) = \frac{1}{{\Delta T}}\sum\limits_{n =  - \infty }^\infty  {\delta \left( {\mu  - \frac{n}{{\Delta T}}} \right)}$ on obtient :
  \[\tilde F(\mu )= \frac{1}{{\Delta T}}\int_{ - \infty }^\infty {F(\tau )\sum\limits_{n = - \infty }^\infty {\delta \left( {\mu - \tau - \frac{n}{{\Delta T}}} \right)} d\tau } \]
  Enfin, après l'intégration :
  \[\boxed{\tilde F(\mu ) = \frac{1}{{\Delta T}}\sum\limits_{n =  - \infty }^\infty {F\left( {\mu  - \frac{n}{{\Delta T}}} \right)} }\]
\end{frame}

\begin{frame}{\insertsubsection}
  Conclusion : la \textbf{transformée de Fourier,}$\tilde F(\mu )$, de la fonction échantillonnée $\tilde f(t)$ \textbf{est une séquence périodique et infinie de copie de $F(\mu )$}, la transformée de Fourier de la fonction continue originale $f(t)$.
  La séparation entre chaque copie est déterminée par $1/\Delta T$.
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image10.png}
    \caption{Transformée de Fourier a) $F(\mu)$, b) over-sampled, c) critically-sampled, d) under-sampled (DIP)}
  \end{figure}
\end{frame}

\subsection{Théorème d'échantillonnage}
\begin{frame}{\insertsubsection}
  Le \textbf{théorème d'échantillonnage} établit la condition nécessaire à la reconstruction d'un signal continue avec un ensemble d'échantillons.
  \begin{itemize}
    \item Avec fonction $f(t)$ dont la transformée de Fourier $F(\mu)$ est borné par $[-\mu_{max},\mu_{max}]$ centré sur l'origine. ($\mu_{max}$ est la fréquence maximale de $f(t)$).
  \end{itemize}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=3.5cm]{content/figures/image12.png}
    \caption{a)$F(\mu)$ b) $\tilde F(\mu)$ critically-sampled  (DIP)}
  \end{figure}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{itemize}
    \item On peut reconstruire adéquatement $f(t)$ seulement si on peut isoler une copie intacte de $F(\mu)$ dans  $\tilde F(\mu)$.
    Conséquemment :
    \[\boxed{\frac{1}{{\Delta T}} > 2{\mu _{\max }}}\]
    En claire, la fréquence d'échantillonnage ($1/\Delta T$) doit être supérieur à deux fois la fréquence maximale $\mu_{max}$ du signal $f(t)$ à échantillonner.
    Cette fréquence est appelée fréquence de Nyquist.
    \item Une valeur faible de $1/\Delta T$ ferait en sorte que $\tilde F(\mu)$ fusionne (under-sampled) alors qu'une valeur plus élevée permet une bonne séparation.
    \item Ces observations supposent que $f(t)$ est défini sur l'ensemble $\left[ {- \infty ,\infty } \right]$ ce qui n'est évidemment pas le cas en pratique.
  \end{itemize}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image14.png}
    \caption{Théorème d'échantillonnage b) Low-pass idéal (DIP)}
  \end{figure}
\end{frame}

\subsection{Distorsion de repliement (Aliasing)}
\begin{frame}{\insertsubsection}
  \begin{itemize}
  \item En théorie, la \textbf{distorsion de repliement} apparaît lorsque la fréquence d'échantillonnage est sous la fréquence de Nyquist.
  Dans ce cas les copies dans le domaine fréquentiel se chevauchent et il devient impossible de les isoler.
  \begin{itemize}
    \item En pratique, elle est toujours présente parce que des fréquences indésirables sont introduites lorsqu'on limite la durée de la fonction (c.-à-d. moins que $\left[ { - \infty ,\infty } \right]$).
    \item La distorsion de repliement est donc inévitable lorsqu'on travaille avec des échantillons pris sur une longueur finie.
    Cet effet peut-être réduit en appliquant un lissage sur la fonction avant l'échantillonnage.
  \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image16.png}
    \caption{sous échantillonnage (DIP)}
  \end{figure}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image17.png}
    \caption{Exemple de distorsion de repliement (DIP)}
  \end{figure}
  \begin{itemize}
    \item Si on avait pris $1/\Delta T = 2\mu_{\max}$ à partir du premier point, l'échantillon aurait toujours été 0.
    \item Ici, $\Delta T$ est plus grand que $P_f$ et on voit apparaître un signal basse fréquence.
  \end{itemize}
\end{frame}

\section{DFT}
\subsection{Transformée de Fourier Discrète (DFT)}
\begin{frame}{\insertsubsection}
  La transformée de Fourier d'une fonction échantillonnée sur $\left[ { - \infty ,\infty } \right]$ est une une fonction continue.
  En pratique, on veut travailler avec un nombre fini d'échantillons et on considère que le signal se répète (c.-à-d. est périodique).
  Avec la transformée
  \[\tilde F(\mu ) = \int_{ - \infty }^\infty  {\tilde f(t){e^{ - j2\pi \mu t}}dt} \]
  En substituant $\tilde f(t) = \sum\limits_{n =  - \infty }^\infty  {f(t)\delta (t - n\Delta T)}$
  ,on obtient:
  \[\tilde F(\mu ) = \sum\limits_{n =  - \infty }^\infty  {\int_{ - \infty }^\infty  {f(t)\delta (t - n\Delta T){e^{ - j2\pi \mu t}}dt} }\]
  et après intégration avec la propriété de sifting (pour $f_n$)
  \[ \boxed{\sum\limits_{n =  - \infty }^\infty  {{f_n} \cdot {e^{ - j2\pi \mu n\Delta T}}} }\]
\end{frame}


\begin{frame}{\insertsubsection}
  Même si $f_n$ est discret, $\tilde F(\mu )$ est toujours continue et infiniment périodique avec une période $1/\Delta T$.
  Pour caractériser la FT on a besoin d'une seule période et échantillonner cette période est l'essence de la
  DFT.\\ Si on veut $M$ échantillons de $\tilde F(\mu )$ entre $\mu=0$ et $\mu=1/\Delta T$ on prend un échantillon aux fréquences :
  \[\mu  = \frac{m}{{M\Delta T}} \hspace{0.5cm} m = 0,1,2,3,...,M - 1\]
  En substituant $\mu$ dans l'expression de la page précédente et en notant le résultat $F_m$:
  \[{F_m} = \sum\limits_{n = 0}^{M - 1} {{f_n} \cdot {e^{ - j2\pi mn/M}}}
  \hspace{0.5cm} m = 0,1,2,3,...,M - 1\]
\end{frame}

\begin{frame}{\insertsubsection}
  Cette expression décrit la DFT, étant donné $M$ échantillons ${f_n}$ de $f(t)$ on obtient $M$ coefficients complexes discrets de la DFT. La transformée de Fourier inverse(IDFT) est donné par:
  \[{f_n} = \frac{1}{M}\sum\limits_{n = 0}^{M - 1} {{F_m} \cdot {e^{j2\pi mn/M}}}
  \hspace{0.5cm} n = 0,1,2,3,...,M - 1\]
  Réaménagé pour la clarté, ces expressions deviennent :
  \[\boxed {F(u) = \sum\limits_{x = 0}^{M - 1} {f(x) \cdot {e^{ - j2\pi ux/M}}}\hspace{0.5cm} u = 0,1,2,3,...,M - 1}\]
  \[\boxed{f(x) = \frac{1}{M}\sum\limits_{u = 0}^{M - 1} {F(u) \cdot {e^{j2\pi ux/M}}}\hspace{0.5cm}x = 0,1,2,3,...,M - 1}\]
\end{frame}

\begin{frame}{\insertsubsection}
  On peut généraliser tous ces résultats en 2D.
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image20.png}
    \caption{Dirac 2D (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image23.png}
    \caption{Train d'impulsions 2D (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image25.png}
    \caption{Distorsion de repliement 2D b) under-sampled (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image27.png}
    \caption{Exemple de distorsion de repliement 2D a)16, b)6, c)0.91, d)0.47 pixel (DIP)}
  \end{figure}
  Notez que d) ressemble à une image normale dont les carrées mesures près de 10 pixels alors que la taille des carrés est de 0.47 pixel.
  \end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image28.png}
    \caption{Exemple de distorsion de repliement 2D a)Aliasing négligeable, b)scaling 50\% en gardant 1/2 pixel , c) même chose après un moyenneur 3x3 (DIP)}
  \end{figure}
  \begin{itemize}
  \item Éliminer un pixel sur deux (c.-à-d. sans filtrage) pour réduire la taille d'une image est équivalent à ré-échantillonner avec une fréquence deux fois moins élevée.
  \end{itemize}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=9cm]{content/figures/image29.png}
    \caption{a) CG image, b) réduit à 25\% avec interpolation biliaire c) même chose après un moyenneur 5x5 (DIP)}
  \end{figure}
  L'image b) présente plusieurs artefacts même avec l'utilisation d'une interpolation; on doit appliquer un filtre passe-bas (c) pour les éliminer.
\end{frame}

\begin{frame}{\insertsubsection}
  La transformée de \textbf{Fourier discrète 2D} pour une image $MxN$ devient :
  \[\boxed{F(u,v) = \sum\limits_{x = 0}^{M - 1} {\sum\limits_{y = 0}^{N - 1} {f(x,y) \cdot {e^{ - j2\pi \left( {ux/M + vy/N} \right)}}} }} \]
  où $u = 0,1,2,3,...,M - 1$ et $v = 0,1,2,3,...,N - 1$\\
  et la transformée inverse:
  \[\boxed{f(x,y) = \frac{1}{{MN}}\sum\limits_{u = 0}^{M - 1} {\sum\limits_{v = 0}^{N - 1} {F(u,v) \cdot {e^{j2\pi \left( {ux/M + vy/N} \right)}}} } }\]
  où $x = 0,1,2,3,...,N - 1$ et $y = 0,1,2,3,...,M - 1$
\end{frame}

\begin{frame}{\insertsubsection}
  Une des propriétés des la DFT implique que la DTF d'un signal $f(x)$ multiplié par $(-1)^x$ subisse une translation de $M/2$.
  On se sert de cette propriété pour \emph{replacer les coefficients} de TF de façon plus pratique.
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=4cm]{content/figures/image37.png}
    \caption{a) 1D DFT, b) DFT shifté, c) 2D DFT, d) DFT shifté (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  Parce les coefficients de la DFT sont complexe :
  \[F(u,v) = \left| {F(u,v)} \right|{e^{j\phi (u,v)}}\]
  où le module est :
  \[\boxed{\left| {F(u,v)} \right| =  \sqrt{ {{R^2}(u,v) + {I^2}(u,v)}}}\]
  et la phase:
  \[\boxed{\phi (u,v) = \arctan \left[ {\frac{{I(u,v)}}{{R(u,v)}}} \right]}\]
\end{frame}

\begin{frame}{\insertsubsection}
  On remarque aussi que :
  \[\left| {F(0,0)} \right| = \frac{1}{MN}MN\sum\limits_{x = 0}^{M - 1} {\sum\limits_{y = 0}^{N - 1} {f(x,y)} } = MN \bar f(x,y)\]
  ${F(0,0)}$ \emph{est proportionnel à la valeur moyenne},$\bar f(x,y)$, de $f(x,y)$ :
  \[\boxed{\left| {\bar f(x,y)} \right| = \frac{{\left| {F(0,0)} \right|}}{{NM}}}\]
  En élé et en une dimension, on appelle généralement ce terme \og composante DC \fg{}. $F(\omega)$ tel que $\omega = 0$.
\end{frame}

\begin{frame}{\insertsubsection}
  Exemple de DFT:
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image41.png}
    \caption{a) Original, b) DFT, c) DFT shifté, d) LOG de DFT shifté (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  Propriété de translation/rotation de la DFT:
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image43.png}
    \caption{a,b )Translation c,d) rotation (DIP)}
  \end{figure}
  La translation ne change pas le spectre de façon apparente.
\end{frame}

\section{Filtrage DFT}
\subsection{Filtrage en DFT}
\begin{frame}{\insertsubsection}
  Étant donné la propriété de convolution le \textbf{filtrage peut être défini comme un simple produit dans le domaine de Fourier} :
  \[g(x,y) = {\Im ^{ - 1}}\left\{ {H(u,v)F(u,v)} \right\}\]
  où $H(u,v)$ est la fonction de transfert du filtre.
  \\~\\
  Par contre, \textbf{la périodicité dans le domaine spatial imposé par la DFT fait en sorte qu'on doit ajouter un padding} pour éviter les erreurs de bouclage (wraparound errors).
  Par exemple, pour la convolution suivante :
  \[f(x)\star h(x) = \sum\limits_{m = 0}^{399} {f(x)h(x - m)} \]
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image47.png}
    \caption{Convolution,à gauche forme idéal, à droite en considérant la périodicité  (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image61.png}
    \caption{a) Original, b) Gaussian blur sans padding, c) avec padding (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  Avec une image $f(x,y)$ et un filtre $h(x,y)$ de taille $M\times N$ la taille $P\times Q$ après padding devrait être $P\geq 2M-1$ et $Q\geq 2N-1$. Le padding doit être appliqué a $f$ et $h$.
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image62.png}
    \caption{a) périodicité sans padding, avec padding (lignes blanches ont été ajouté pour la clarté) (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image57.png}
    \caption{Exemple de DFT (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image58.png}
    \caption{Résulta de mettre le terme $F(0,0)$ ou, après shifting, $F(M/2,N/2)=0$ (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image60.png}
    \caption{a) passe-bas, b) passe-haut, c) l'espace $a$ produit un filtre de sharpening (DIP)}
  \end{figure}
\end{frame}

\subsection{Résumé filtrage DFT}
\begin{frame}{\insertsubsection}
  \begin{enumerate}
    \item Obtenir les paramètres $P$ et $Q$ à partir la taille $M\times N$ de $f(x,y)$ avec $P=2M$ et $Q=2M$.
    \item Obtenir l'image paddée, $f_p(x,y)$ de taille $P\times Q$ en ajoutant les zéros nécessaires vers la droite et vers le bas.
    \item Multiplier  $f_p(x,y)$ par $(-1)^{x+y}$ pour centrer la transformée.
    \item Calculer la DFT $F(u,v)$
    \item Générer un filtre symétrique $H(u,v)$ de taille $P\times Q$ centré sur $(P/2, Q/2)$.
    \item Calculer $G(u,v)=H(u,v)F(u,v)$
    \item Faire l'IDFT avec le décalage inverse : $g_p(x,y)=\{\text{real}[ {\Im ^{ - 1}}G(u,v)  ]    \}(-1)^{x+y}$
    \item Obtenir $g(x,y)$ en rognant la région $M \times N$ supérieure gauche de $g_p(x,y)$
  \end{enumerate}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image67.png}
    \caption{a) original, b) padding, c) multiplié par $(-1)^{x+y}$, d)DF, e) Filtre gaussien, f) produit de $d$ et $e$, g) $g_p(x,y)$, g) $g(x,y)$ (DIP)}
  \end{figure}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image73.png}
    \caption{a) Filtre de Sobel et son équivalent après DFT b) image DF du Sobel, c) filtrage fréquentiel, d) filtrage spatial identique à c)(DIP)}
  \end{figure}
\end{frame}

\section{Type de filtres ~~~~~}
\subsection{Passe-bas idéal}
\begin{frame}{\insertsubsection}
  Le filtre passe-bas idéal (ILPF) coupe toute fréquence au-delà de la fréquence de coupure $D_0$ et est défini comme :
\[H(u,v) = \left\{ {\begin{array}{*{20}{c}} 1&{{\text{if }}D(u,v) \leqslant {D_0}} \\ 0&{{\text{if }}D(u,v) > {D_0}} \end{array}} \right.\]
avec :
\[D(u,v) = {\left[ {{{\left( {u - P/2} \right)}^2} + {{\left( {v - Q/2} \right)}^2}} \right]^{1/2}}\]
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image75.png}
    \caption{ILPF (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image76.png}
    \caption{a) original, b) spectre avec les différents rayons utilisé dans la figure suivante pour ILPF(DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=5cm]{content/figures/image77.png}
    \caption{a) original($688\times 688$), b-f)  ILPF $D_0=10,30,60,160,460$  (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8cm]{content/figures/image78.png}
    \caption{ILFP dans le domaine spatial  (DIP)}
  \end{figure}
  \begin{itemize}
  \item La discontinuité du filtre (équivalent à la porte en 1D) dans le domaine de Fourier engendre des composantes fréquentielles infinies (sinus cardinal) dans  le domaine spatial.
  Ce filtre produit un effet de \og riging \fg{} observé à la figure précédente.
  \end{itemize}
\end{frame}


\subsection{Passe-bas Butterworth}
\begin{frame}{\insertsubsection}
  Le filtre passe-bas Butterworth (BLPF) d'ordre $n$ avec une fréquence de coupure $D_0$ est défini comme :
  \[H(u,v) = \frac{1}{{1 + {{\left[ {D(u,v)/{D_0}} \right]}^{2n}}}}\]
  On constate qu'à $D_0$ les coefficients sont réduits à 50\%.
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image80.png}
    \caption{BLPF (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=5cm]{content/figures/image77.png}
    \caption{a) original($688\times 688$), b-f)  BLPF $D_0=10,30,60,160,460$  (DIP)}
  \end{figure}
\end{frame}

\subsection{Passe-bas gaussien}
\begin{frame}{\insertsubsection}
  Le filtre passe-bas gaussien (GLPF) avec une fréquence de coupure $D_0$ équivalente à $\sigma$ est défini comme :
  \[H(u,v) = {e^{ - \frac{{{D^2}(u,v)}}{{2D_0^2}}}}\]
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image83.png}
    \caption{GLPF (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=5cm]{content/figures/image85.png}
    \caption{a) original($688\times 688$), b-f)  BLPF $D_0=10,30,60,160,460$  (DIP)}
  \end{figure}
\end{frame}


\subsection{Passe-haut}
Les passe-haut simple sont défini comme l'inverse des passe-bas.
\begin{frame}{\insertsubsection}
  \begin{itemize}
  \item Idéal
    \[H(u,v) = \left\{ {\begin{array}{*{20}{c}} 0&{{\text{if }}D(u,v) \leqslant {D_0}} \\ 1&{{\text{if }}D(u,v) > {D_0}} \end{array}} \right.\]
  \item Butterworth
    \[H(u,v) = \frac{1}{{1 + {{\left[ {{D_0}/D(u,v)} \right]}^{2n}}}}\]
  \item Gaussien
    \[H(u,v) = 1 - {e^{ - \frac{{{D^2}(u,v)}}{{2D_0^2}}}}\]
  \end{itemize}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=7cm]{content/figures/image90.png}
    \caption{HPF a) Idial, b) Butterworth, c) Gaussian  (DIP)}
  \end{figure}
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8.5cm]{content/figures/image92.png}
    \caption{IHPF $D_0=30,60,160$  (DIP)}
  \end{figure}
\end{frame}
\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8.5cm]{content/figures/image93.png}
    \caption{BHPF $D_0=30,60,160$  (DIP)}
  \end{figure}
\end{frame}
\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8.5cm]{content/figures/image94.png}
    \caption{GHPF $D_0=30,60,160$  (DIP)}
  \end{figure}
\end{frame}



\begin{frame}{\insertsubsection}
  \begin{itemize}
  \item Filtre Laplacien
    \[H(u,v) =  - 4{\pi ^2}{D^2}(u,v)\]
  \end{itemize}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=6cm]{content/figures/image97.png}
    \caption{Sharpening laplacien  (DIP)}
  \end{figure}
\end{frame}

\subsection{Passe-bande}
\begin{frame}{\insertsubsection}
  \begin{itemize}
    \item Idéal
    \[H(u,v) = \left\{ {\begin{array}{*{20}{c}} 0&{{\text{if }}{D_0} - W/2 \leqslant D \leqslant {D_0} + W/2} \\ 1&{{\text{otherwise}}} \end{array}} \right.\]
    \item Butterworth
    \[(u,v) = \frac{1}{{1 + {{\left[ {\frac{{DW}}{{{D^2} - D_0^2}}} \right]}^{2n}}}}\]
    \item gaussien
    \[H(u,v) = 1 - {e^{ - {{\left[ {\frac{{{D^2} - D_0^2}}{{DW}}} \right]}^2}}}\]
  \end{itemize}
  avec $D$, la distance $D(u,v)$; $W$, sa largeur et $D_0$, la fréquence de coupure
\end{frame}

\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=8.5cm]{content/figures/image106.png}
    \caption{a) band-reject b) band-pass (DIP)}
  \end{figure}
\end{frame}


\subsection{Autre exemple}
\begin{frame}{\insertsubsection}
  \begin{figure}[!ht]
    \centering
    \includegraphics[width=5cm]{content/figures/image108.png}
    \caption{a) Original, b) DF, c) DF après filtrage, d) résultat (DIP)}
  \end{figure}
\end{frame}


\end{document}
