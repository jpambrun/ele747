#!/bin/bash
latexmk Presentation.tex -jobname=${PWD##*/}_Presentation
latexmk Handout.tex -jobname=${PWD##*/}_Handout 
latexmk PresentationOverlay.tex -jobname=${PWD##*/}_PresentationOverlay

rm -f *.nav *.snm *.vrb *.log *.aux *.fdb_latexmk *.out *.toc *.gz
mv *.pdf ./../published
