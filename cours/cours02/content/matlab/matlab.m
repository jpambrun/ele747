%% connexité
A=zeros(5:5);A(1:2,1:2)=1;A(3:4,3:4)=1;A(4:5,1:2)=1;
% A =
%  1     1     0     0  
%  1     1     0     0     
%  0     0     1     1
%  1     1     1     1 
%  1     1     0     0

B4= bwlabel(A,4); % 4-connecté
B4= bwlabel(A,8); % 8-connecté


% avec v=[5..7]  
A=[5 7 2 4 7;6 5 1 5 4;4 9 2 8 4; 0 7 5 7 9]
B = A<=7 & A>=5
B4= bwlabel(A,4); % 4-connecté
B4= bwlabel(A,8); % 8-connecté


%% Chemins et contours
A = ...
    [1     1     0     0;...
     1     1     1     0;... 
     1     0     0     1;...
     1     1     1     1;...
     1     1     0     0] 
 
Ab = bwtraceboundary(A, [1 1], 'N',8, 100,'clockwise') % retourne le contour sous forme de liste de points Ab

imshow(A,[],'InitialMagnification',3000) %affichage de A avec zoom 30x
hold on;  plot(Ab(:,2),Ab(:,1),'g','LineWidth',2); % affichage de Ab


% Exemple d'application des chemins
I = imread('circuit.tif');
BW = edge(I,'canny'); % trouve les contours
imshow(BW,'InitialMagnification',200); % affichage zoom 2x

BWb = bwtraceboundary(BW, [242 197], 'N',4, 1000,'clockwise') % retourne le contour sous forme de liste de points Ab
hold on;  plot(BWb(:,2),BWb(:,1),'g','LineWidth',2);


%% Plage dynamique et opération ponctuelle 
I = dicomread('000150.dcm'); %j'ai fournis cette image dans le .zip
imshow(I) % affichage problématique puisque matlab assume une plage dynamique de [0 65535] (uint16) alors que c'est plutôt ~[0 1800]
imshow(I,[]) % dans ce cas matlab utilise [min(I) max(I)]
imshow(I,[0 1755]) % == ligne d'avant
imshow(I,[-150 300]) % spécification d'une plage dynamique aléatoire


%% Opération sur les plan binaires
I = double(imread('cameraman.tif'));
imshow(bitget(I,8)) % affichage d'un seul plan binaire
imshow([I bitset(I,3,0)],[]) % Mise a zéro du 3eme bit le moins significatif


%% Calculs de l'exemple d'égalisation d'histogramme 
A=[.19 .25 .21 .16 .08 .06 .03 .02]
B=cumsum(A);
C=B*7;
round(C)

% Exemple d'application
I = imread('pout.tif');
imshow(I);
imhist(I);
Ieq = histeq(I);
imshow([I Ieq]);



%% histogrammes
I = dicomread('000150.dcm');
imtool(I) % outils interactif d'histogramme de et d'étirement de contraste




