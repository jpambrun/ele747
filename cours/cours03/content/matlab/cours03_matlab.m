%% trans affine
clear all;
I=double(imread('cameraman.tif'))/255;

% zoom 50%
T1=[ .5   0  0;
      0  .5  0;
      0   0  1 ];
% translation 
T2=[ 1   0    0;
     0   1    0;
     128 128   1 ];
 
% rotation
T3=[ cos(pi/4)    sin(pi/4)     0;
    -sin(pi/4)    cos(pi/4)     0;
     0            0             1] ;
 
% combinaisoin des 3
TT= T1^-1 * T2^-1 * T3^-1;

% créer les matrices de coordonnées v,w
[v,w]=meshgrid(1:size(I,1), 1:size(I,2));
vy1 = [v(:) w(:) ones(numel(I),1)];  
    % figure;imshow([v w],[])

  
% a) Foward mapping
xy1 = vy1 * T1;
x = reshape(xy1(:,1), size(I));
y = reshape(xy1(:,2), size(I));
    % figure;imshow([x y],[0 512]);title('matrices contenant les coordonnees ou devront ALLER les pixels dans l''image TRANFORME')
It= interp2(x,y,I, v, w);
imshow([I It]);

% b) Inverse mapping
xy1 = [v(:) w(:) ones(numel(I),1)] * T1^-1;
  % xy1 = [v(:) w(:) ones(numel(I),1)] * T2^-1;
  % tic;xy1 = [v(:) w(:) ones(numel(I),1)] * T1^-1 * T2^-1 * T3^-1;toc
  % tic;xy1 = [v(:) w(:) ones(numel(I),1)] * T3^-1 * T1^-1 * T2^-1;toc
  % tic;xy1 = [v(:) w(:) ones(numel(I),1)] * TT;toc
x = reshape(xy1(:,1), size(I));
y = reshape(xy1(:,2), size(I));
    % figure;imshow([x y],[0 512]);title('matrices contenant les coordonnees d'ou VIENNENT les pixels dans l''image ORIGINALE')
It= interp2(I, x, y);
figure;imshow([I It]);


%% Interpolation 
clear all;
x =  [0  1 2 3 4 5 6 7 8 9];
y =  [10 5 7 3 9 1 2 3 4 9];
subplot(5,1,1);plot(x,y, '-o');

xi = 0:.1:9;
subplot(5,1,2);yi = interp1(x,y,xi, 'linear');plot(xi,yi, '-o');%default
subplot(5,1,3);yi = interp1(x,y,xi, 'nearest');plot(xi,yi, '-o');
subplot(5,1,4);yi = interp1(x,y,xi, 'cubic');plot(xi,yi, '-o');
subplot(5,1,5);yi = interp1(x,y,xi, 'spline');plot(xi,yi, '-o');


%% padding
clear all;
A = [1 2;
     3 4]
Az = padarray(A,[2 2]) % zéro
Ap = padarray(A,[2 2],'circular') % périodique
As = padarray(A,[2 2],'symmetric') % symétrique (miroir)

%% Convolution & corrélation 1D
% zero padding
clear all;
f=randi(4,[1,6])
w=[1 2 3];

% filter2(w, f, 'same')
% conv2(f,w,'same')
% filter2(w, f, 'full')
% conv2(f,w,'full')

%% Convolution & corrélation 2D
% zero padding
clear all;
f=randi(5,[4 4])
w=[0 0 1;
   0 2 0;
   0 0 0];
% filter2(w,f,'same')
% conv2(f,w,'same')
% filter2(w,f,'full')
% conv2(f,w,'full')

fp=padarray(f,[1 1],'circular')
filter2(w,fp,'valid')
conv2(fp,w,'valid')

%% Filtre moyenneur & importance du padding
clear all;
f = ones(6,18);
f(:,7:15)=0;
subplot(3,1,1);imshow(f,'InitialMagnification',3000)

w = ones(3,3) / 3.^2
fw= filter2(w,f)
subplot(3,1,2);imshow(fw,'InitialMagnification',3000)

fp=padarray(f,[1 1],'circular')
fwp= filter2(w,fp,'valid')
subplot(3,1,3);imshow(fwp,'InitialMagnification',3000)

%% Filtre médian & comparaison
clear all;
f(1:20,1:100)=0.5;
f(:,1:70) = repmat(linspace(0,1,70), 20,1);
f(:,75)=1;      % 1px
f(:,82:83)=1;   % 2px
f(:,90:92)=1;   % 3px

% imshow(f,'InitialMagnification',500)
fsa = imnoise(f, 'salt & pepper');
fga = imnoise(f, 'gaussian');
fsp = imnoise(f, 'speckle');
fn = [fsa fga fsp];


% imshow(fn,'InitialMagnification',300)
fn_moy  = filter2(ones(3,3)/9, fn);
fn_med  = medfilt2(fn, [3 3]);
fn_gaus = filter2(fspecial('gaussian',3,1), fn);
imshow([fn;fn_moy;fn_med;fn_gaus],'InitialMagnification',300)
xlabel('bruit : S&P, gaussian, speckle')
ylabel('guss     med    moy   noisy')