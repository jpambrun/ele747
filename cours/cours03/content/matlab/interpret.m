%% histogramme
I=imread('cameraman.tif');
Id=double(I);
Ieq=double(histeq(I));

subplot(2,2,1);
imshow(I,[0 255])

subplot(2,2,2);
imshow(Ieq, [0 255]);

subplot(2,2,3);
hold on;
bar(hist(Id(:),256)/numel(I)*30)
plot(cumsum(hist(Id(:),256))/numel(I))

subplot(2,2,4)
hold on;
bar(hist(Ieq(:),256)/numel(I)*30)
plot(cumsum(hist(Ieq(:),256))/numel(I))

%% trans affine
I=double(imread('cameraman.tif'))/255;

Tc=[1 0 0;0 1 0;-128 -128 1];
Tc2=[1 0 0;0 1 0;+128 +128 1];

T1=[0.5 0 0;0 0.5 0;0 0 1];
T2=[1 0 0;0 1 0;128 128 1];
T3=[cos(pi/4) sin(pi/4) 0;-sin(pi/4) cos(pi/4) 0;0 0 1];
TT= T1 * T2 * T3;

[v,w]=meshgrid(1:size(I,1), 1:size(I,2));

xy = [v(:) w(:) ones(numel(I),1)]   * T2^-1 * T1^-1 * T3^-1;%inverse mapping 


x1= xy(:,1);
y1= xy(:,2);

x = reshape(x1,size(I));
y = reshape(y1,size(I));

It= interp2(I, x, y);
imshow([I It]);


%% inverse
T1=[2 0 0;0 2 0;0 0 1];
T2=[1 0 0;0 1 0;-128 -128 1];
T3=[cos(-pi/4) sin(-pi/4) 0;-sin(-pi/4) cos(-pi/4) 0;0 0 1];

xy2 = [v(:) w(:) ones(numel(I),1)]    *T3^-1* T1^-1 * T2^-1;
x1= xy2(:,1);
y1= xy2(:,2);

x = reshape(x1,size(I));
y = reshape(y1,size(I));

Itt= interp2(It, x, y);
imshow([I It Itt]);

%% convolution
m=randi(9,6);
w=ones(3,3);w(2,2)=2;w=w/sum(sum(w));
filter2(w,M);

%% filtre moyenneur
I=imread('cameraman.tif');
w33=ones(3,3)./9;
 w77=ones(7,7)./(7*7);
G3=filter2(w33,I);
 G7=filter2(w77,I); %attention zero-padding
imshow([I G3 G7],[0 255]);

%% filtre gaussien
[X,Y] =meshgrid(-2:2,-2:2)
wg=1/(2*pi*1)*exp(-(X.^2+Y.^2)/(2*1^2))
D=sqrt(X.^2+Y.^2); %petite note..

%% présentation laboratoire

%imshow(I, n)
xx=[0:.01:2*pi];yy=sin(xx);
plot(xx,yy);
title('Figure title');
legend('line 1')
print -depsc fig