%% mesure de qualite
clear all;close all;
T = 9000; %threshold (9000)
I=imread('pout.tif');

% mise a zero de coefficient Fourier
F=fft2(I);
zero = numel(find(abs(F)<T))/numel(F)
F2=F;F2(abs(F)<T)=0;
imshow(log(1+abs([F F2])),[]);
I2=uint8(ifft2(F2));
figure;imshow([I I2]);

% mesure de qualite
mse  = MSE(I, I2)
snr  = SNR(I, I2)
psnr = PSNR(I, I2)

[psnr_matlab, mse_matlab] = measerr(I,I2)

% PSNR pas identique pcq measerr considere un range de 255 alors que PSNR
% considere le range (max - min) de I.

%% quantification
I=imread('cameraman.tif');

% poor person's quantification
Iq=(I/8)*8;

imshow([I Iq])
entropy(I)
entropy(Iq)



%% DPCM predictif
clear all;
close all;
I=imread('pout.tif');
Iv=[0;I(:)];
Ivd=diff(int16(Iv));%range de 512

figure;imhist(I);
figure;imhist(uint8(Ivd+127));

Hid=[mon_entropy(I) mon_entropy(double(Ivd))]
figure;imshow( reshape( Ivd , size(I)) ,[])
Ir = reshape(cumsum(double(Ivd)), size(I));
figure;imshow(  Ir ,[0 255])

%% DFT DCT energy
I=double(imread('pout.tif'));
sum(sum(norm(I).^2))
sum(sum(1/numel(I) * norm(fft2(I)).^2))
sum(sum( norm(dct2(I)).^2) )
figure;mesh(abs(dct2(I)));
figure;mesh(abs(fft2(I)));
figure;mesh(abs(I));

%% compression avec DCT
clear all;
T = 30; %threshold
I=imread('pout.tif');

F=dct2(I);
zero = numel(find(abs(F)<T))/numel(F)
F2=F;F2(abs(F)<T)=0;
figure;imshow(log(1+abs([F F2])),[]);

I2=uint8(idct2(F2));
figure;imshow([I I2]);
[PSNR,MSE] = measerr(I,I2)

