function [ mse ] = MSE( I1, I2 )
    I1=double(I1);
    I2=double(I2);
    mse=1/numel(I1) * sum((I1(:)-I2(:)).^2);
end

