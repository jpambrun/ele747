function [ output_args ] = mon_entropy( in )
    in = double(in);
    count = histc(in(:),-2048:1:2048);
    px= count / numel(in);
    px_nz= px(find(count));
    output_args=-sum(px_nz.*log2(px_nz));
end