function [ snr ] = SNR( I1,I2 )
    I1=double(I1);
    I2=double(I2);
    snr = sum(I1(:).^2) ./ (MSE(I1,I2) .* numel(I1));
end