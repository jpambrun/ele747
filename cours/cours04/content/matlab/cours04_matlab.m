%% associatif
clear all;clc;
I=im2double(imread('cameraman.tif'));

h = [1 1 1;1 1 1; 1 1 1];
h2 = filter2(h,h,'full');

tic;
Ih1 = filter2(h,filter2(h,I)); 
toc;
tic;
Ih2 = filter2(h2,I);
toc;

Id = abs(Ih1-Ih2);
imshow(Id,[]);

%% seperabilite  
h1 = filter2([1 1 1],[1 1 1]','full')
h2 = filter2([1 2 3 2 1],[1 2 3 2 1]','full')


%% Derive 1re et 2me en x
I=imread('cameraman.tif');

h=[0,0,0;-1,1,0;0,0,0];
Ic=filter2(h,I,'same');
imshow(Ic,[]);

figure;
h2=[0,0,0;1,-2,1;0,0,0];
Ic2=filter2(h2,I,'same');
imshow([Ic Ic2],[]);

%% Derive 1re et 2me en y
I=imread('cameraman.tif');
figure;
h=[0,0,0;0,-1,0;0,1,0];
Ic=filter2(h,I,'same');
imshow(Ic,[]);

figure;
h2=[0,1,0;0,-2,0;0,1,0];
Ic2=filter2(h2,I,'same');
imshow([Ic Ic2],[]);

%% Derive 1re en x et y
I=imread('cameraman.tif');

hx=[0,0,0;0,-1,1;0,0,0];
hy=[0,0,0;0,-1,0;0,1,0];
Ix=filter2(hx,I,'same');
Iy=filter2(hy,I,'same');
imshow([I abs(Ix)*5 abs(Iy)*5 (abs(Ix) +abs(Iy))*2]);


%% Profil avec step et rampe
h=[0,0,0;0,-1,1;0,0,0];
h2=[0,0,0;1,-2,1;0,0,0];
clear A;
A(1:25)=0;
A(12:17)=10;
A(26:50)=1:8:200;
A(51:60)=200;
A(61:90)=200:-5.1:50;
A(91:100)=50;
A=[A;A];A=[A;A];A=[A;A];A=[A;A];A=[A;A];
Ac=filter2(h,A,'valid');
Ac2=filter2(h2,A, 'valid');

subplot(3,2,1); imshow(A,[]);
subplot(3,2,3);imshow(Ac,[]);
subplot(3,2,5);imshow(Ac2,[]);
subplot(3,2,2);plot(A(10,:),'-k.');
subplot(3,2,4);plot(Ac(10,:),'-k.');
subplot(3,2,6);plot(Ac2(10,:),'-k.');

%% cerlce derivee en diag. (Roberts)
[Ix, Iy] = meshgrid(-127:127,-127:127);
R = sqrt(Ix.^2+Iy.^2);
Rd=zeros(size(Ix));
Rd(R>100)=255;
% imshow(Rd)

Rd=filter2(fspecial('gaussian',5,1),Rd)

hx=[0,0,0;0,-1,1;0,0,0];
hy=[0,0,0;0,-1,0;0,1,0];
% imshow(Rd,[]);
% plotepstex(gcf,'cercle.eps',[9,1,10]);
figure;
imshow([abs(filter2(hx,Rd)) abs(filter2(hy,Rd))],[0 100]);colormap(jet)

figure;
hd1=[0,0,0;0,-1,0;0,0,1];
hd2=[0,0,0;0,-1,0;1,0,0];
imshow([abs(filter2(hd1,Rd)) abs(filter2(hd2,Rd))],[0 100]);colormap(jet)
%plotepstex(gcf,'cercle_d.eps',[9,2,10]);

%% laplacien
I=imread('cameraman.tif');
L=[1 1 1; 1 -8 1; 1 1 1];
imshow( (conv2(I,L)),[]);

%% freq
Hl=[1 1 1;1 -8 1; 1 1 1];
freqz2(Hl); %affiche la reponse frequentiel equiv a faire Fourier padde avec des zeros

%% LoG (laplacien de gaussienne)
clear all;
[x,y]=meshgrid(-3:1:3,-3:1:3);
sigma=1.4;

log1=((x.^2+y.^2-2*sigma^2)/(sigma^4)) .* exp(-((x.^2 + y.^2)/(2*sigma^2)))

I=im2double(imread('cameraman.tif'));
imshow(conv2(I,log1),[] );

%% unsharp filter (masque flou)
close all;clear all;
I=im2double(imread('cameraman.tif'));

G=fspecial('gaussian',7,2);
Ig = conv2(I,G,'same');


Ius=I+(I-Ig);

imshow([I(:,1:117) Ius(:,118:175) Ius(:,176:256)]);truesize;


Hi=zeros(7,7);Hi(4,4)=1;% Identite
H2= 2.*Hi-G;
Ius2= conv2(I,H2,'same');
imshow(abs(Ius2-Ius),[0 .001])


%% laplacien sharpening
I=im2double(imread('pout.tif'));
H=[1 1 1; 1 -8 1; 1 1 1];
Io= I + (-0.2)*conv2(I,H,'same');
imshow([I Io])