function [ psnr ] = PSNR( I1, I2 )
    I1=double(I1);
    I2=double(I2);
    psnr=10*log10( ( max(I1(:))  - min(I1(:)) )^2 / MSE(I1,I2));
end

