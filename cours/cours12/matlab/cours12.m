%% Filtrage FFT
I=rgb2gray(imread('tv.png'));
F=fft2(I);
Fs=fftshift(F);


% 4 petites regions 
H=ones(size(Fs));
%remove comment
%H(120,352)=0;
%H(181,352)=0;
%H(363,352)=0;
%H(420,352)=0;
se=strel('disk',10,0);
H=imerode(H,se);

% passe bas
H2=zeros(size(Fs));
H2(271,352)=1;
se=strel('disk',170,8);
H2=imdilate(H2,se);

% Filtrage
Fsf=Fs.*H.*H2;

% affichage fig1: Fourier avant/apres. fig2
figure;imshow(log(abs([Fs Fsf])+1),[]);
xlabel('a) Fourier, b) resultat du fultrage')

figure;imshow([I ifft2(ifftshift(Fsf))],[]);
xlabel('a) Originale, b) Filtree')

%% FFT sharpening
I=rgb2gray(imread('peppers.png'));
F=fft2(I);
Fs=fftshift(F);

%distance du centre
[M,N]=size(I);[x,y]= meshgrid(-N/2:(N-1)/2, -M/2:(M-1)/2);
D=sqrt(x.^2+y.^2);

% HP butterworth
Hhp=1./(1+(100./D).^2); 

% sharpening 
% on peut changer le facteur (f)
f=1;
Hs=1+f*Hhp; 

subplot(1,2,1);mesh(Hhp);axis([0 600 0 400 0 3])
title('hp butterwoth')
subplot(1,2,2);mesh(Hs);axis([0 600 0 400 0 3])
title('sharpening butterwoth')

figure;imshow([I ifft2(ifftshift(Fs.*Hs))])
xlabel('a) Originale, b) Filtree')

%% chroma downsampling
I = imread('peppers.png');

% facteur de down-sampling
f= 1/4;

% conversion YCBCR
YCBCR = rgb2ycbcr(I);
Y  = YCBCR(:,:,1);
CB = YCBCR(:,:,2);
CR = YCBCR(:,:,3);

% subsampling de CB et CR
CB2= imresize(imresize(CB, f), size(Y));
CR2= imresize(imresize(CR, f), size(Y));%on peut changer le facteur, interpolation bicubic + anti-aliasing

% affichage
I2= ycbcr2rgb(cat(3,Y,CB2,CR2));
imshow([I I2]);
xlabel('a) Originale, b) couleur sous echantillonne')
%% DCT 8x8 - codage & mode progressif
clear all;
I=rgb2gray(imread('peppers.png'));

% unniform quantiser (ex /q *q)
q=1;

% Céer un mask 8x8
H=int16(zeros(8,8));

% uncomment pour conserver plus de coef
H(1,1)=1;
H(1,2)=1;H(2,1)=1;
%H(2,2)=1;H(1,3)=1;H(3,1)=1;
%H(4,1)=1;H(3,2)=1;H(2,3)=1;H(1,4)=1;

f_filter   = @(x) x.data.*H /q*q; 

dct =  int16(blkproc(I,[8 8],@dct2));
dctf  = blockproc(dct,[8 8],f_filter);
Iq    = blkproc(dctf,[8 8],@idct2);

figure;imshow([I Iq]);
[mon_entropy(I) mon_entropy(dctf)]

%% DCT 8x8 quant
clear all;

% scaling factor de Qt
f=1;

% matrice standard jpeg
Qt = int16([  16  11  10  16  24  40  51  61 ; ...
              12  12  14  19  26  58  60  55 ; ...
              14  13  16  24  40  57  69  56 ; ...
              14  17  22  29  51  87  80  62 ; ...
              18  22  37  56  68 109 103  77 ; ...
              24  35  55  64  81 104 113  92 ; ...
              49  64  78  87 103 121 120 101 ; ...
              72  92  95  98 112 100 103  99 ]);

Qt=f*Qt;  %scaling

% fonction de quantification par bloc
f_quant   = @(x) (x.data)./Qt;
f_unquant = @(x) x.data.*Qt;

% chargement et offset
I     = int16(rgb2gray(imread('peppers.png')));
I     = I - 128;

% dct et quantif par bloc
dct   = int16(blkproc(I,[8 8],@dct2));
dctq  = blockproc(dct,[8 8],f_quant);

% affichage des coefs quantidiés
figure;imshow(abs(dctq),[0 8]);colormap(jet);

% idct et iquantif par bloc
dctuq = blockproc(dctq,[8 8],f_unquant);
Iq    = blkproc(dctuq,[8 8],@idct2);

% affichage
figure;imshow([I+128,Iq+128], [0 255]);
[mon_entropy(I) mon_entropy(dctq)]

%% dtw
% change le type de padding
dwtmode('per');
clear all;close all;

I=int16(rgb2gray(imread('lines.png')));
imshow(I,[]);

% premiere decomposition
[cA,cH,cV,cD] = dwt2(I,'bior3.5');

% seconde decomposition
[cA1,cH1,cV1,cD1] = dwt2(cA,'bior3.5');

figure;imshow(abs([[[[cA1/10 cV1]; [cH1 cD1]] cV]; [cH cD]]),[0 30]);colormap(jet)

%% dwt compression
% change le type de padding
dwtmode('per');
clear all;

% facteurs de quantification
fq = 1; %2, 3, 4, 5, 6..
q1=int16(16*fq);
q2=int16(8*fq);


I=int16(rgb2gray(imread('peppers.png')));
E_img=mon_entropy(I)

% premiere decomposition
[cA,cH,cV,cD] = dwt2(I,'bior3.5');
%figure;imshow([[cA/10 cH]; [cV cD]],[]);

% seconde decomposition
[cA1,cH1,cV1,cD1] = dwt2(cA,'bior3.5');
%figure;imshow([[[[cA1/10 cH1]; [cV1 cD1]] cH]; [cV cD]],[]);

% entropy
E_dwt_total=mon_entropy([[[[cA1/10 cH1]; [cV1 cD1]] cH]; [cV cD]]) 
% affichage
figure;imshow(abs([[[[cA1/10 cV1]; [cH1 cD1]] cV]; [cH cD]]),[0 30]);colormap(jet)

% quatification
cA1=int16(cA1);%par de quatif sur l'approx
cH1=int16(cH1)/q1*q1;
cV1=int16(cV1)/q1*q1;
cD1=int16(cD1)/q1*q1;
cH=int16(cH)/q2*q2;
cV=int16(cV)/q2*q2;
cD=int16(cD)/q2*q2;

% entropy
E_dwt_total_quatiz=mon_entropy([[[[cA1/10 cH1]; [cV1 cD1]] cH]; [cV cD]])
% affichage
figure;imshow(abs([[[[cA1/10 cV1]; [cH1 cD1]] cV]; [cH cD]]),[0 30]);colormap(jet)

% transforme inverse
Ir1=idwt2(cA1,cH1,cV1,cD1, 'bior3.5');
Ir=idwt2(Ir1,cH,cV,cD, 'bior3.5');

%affichage
figure;imshow([I uint8(Ir)],[])

% PSNR
psnr =PSNR(I, uint8(Ir))
