#!/bin/bash
latexmk -pdf Presentation.tex -jobname=${PWD##*/}_Presentation
latexmk -pdf Handout.tex -jobname=${PWD##*/}_Handout 
latexmk -pdf PresentationOverlay.tex -jobname=${PWD##*/}_PresentationOverlay

rm -f *.nav *.snm *.vrb *.log *.aux *.fdb_latexmk *.out *.toc *.gz
mv *.pdf ./../published
