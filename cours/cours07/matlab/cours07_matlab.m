%% color transform CMYK
RGB = im2double(imresize(imread('207206_9370.jpg'),.3));
CMY=1-RGB;
K=min(CMY,[],3);
CMYK=cat(3,CMY-cat(3,K,K,K),K);

%Y=zeros(size(RGB)); Y(:,:,1)=CMY(:,:,1); Y(:,:,2)=CMY(:,:,1);
figure;imshow(RGB)
figure;imshow([ CMYK(:,:,1) CMYK(:,:,2); CMYK(:,:,3) CMYK(:,:,4)])
xlabel('a) C, b) M, c) Y, d) K')

%% HSV
RGB = imresize(imread('207206_9370.jpg'),.3);
hsv= rgb2hsv(RGB);
figure;imshow(RGB)
figure;imshow([hsv(:,:,1) hsv(:,:,2) hsv(:,:,3)])
xlabel('H S V')

%% color lab vs HSV
RGB = imresize(imread('207206_9370.jpg'),.3);
cform = makecform('srgb2lab');
lab = applycform(RGB,cform);
figure;imshow(RGB)
figure;imshow([mean(RGB,3); lab(:,:,1)]);

%% pseudo color
load spine;
close all;
figure;imshow(X,map);
figure;imshow(X,gray(64));
figure;imshow(X,copper(64));
figure;imshow(X,jet(64));

Xg=ind2gray(X,map); 
X16 = grayslice(Xg,16);% permet de recuperer les slices
figure;imshow(X16,jet(16));

%% traitement
% en RGB
RGB = im2double(imresize(imread('207206_9370.jpg'),.3));
H= 1/25*ones(5,5);
H1= [-1 -1 -1;-1 8 -1;-1 -1 -1]
f(:,:,1)=filter2(H, RGB(:,:,1));
f(:,:,2)=filter2(H, RGB(:,:,2));
f(:,:,3)=filter2(H, RGB(:,:,3));

f1(:,:,1)=filter2(H1, RGB(:,:,1));
f1(:,:,2)=filter2(H1, RGB(:,:,2));
f1(:,:,3)=filter2(H1, RGB(:,:,3));

% en HSV
hsv = rgb2hsv(RGB);
f_hsv=hsv;
f_hsv(:,:,3) = filter2(H, hsv(:,:,3));
f1_hsv(:,:,3) = filter2(H1, hsv(:,:,3));
fh = hsv2rgb(f_hsv);
fh1 = hsv2rgb(f1_hsv);
imshow([f fh; RGB+0.4.*f1 RGB+0.4.*fh1 ]);

%% segmentation HSV
RGB = im2double(imresize(imread('207206_9370.jpg'),.3));
hsv= rgb2hsv(RGB);
imshow(hsv(:,:,1)>.59 & hsv(:,:,1)<.64);

%% segmentation RGB cube
RGB = im2double(imresize(imread('207206_9370.jpg'),.3));
couleur = [.1 .22 .57];
delta = .15;
couleurMax= couleur+delta;couleurMin= couleur-delta;
rgbSegCube= ...
    RGB(:,:,1)<couleurMax(1) & RGB(:,:,1)>couleurMin(1) & ...
    RGB(:,:,2)<couleurMax(2) & RGB(:,:,2)>couleurMin(2) & ...
    RGB(:,:,3)<couleurMax(3) & RGB(:,:,3)>couleurMin(3);
imshow(rgbSegCube);

%% segmentation RGB shpere
RGB = im2double(imresize(imread('207206_9370.jpg'),.3));
couleur = [.1 .22 .57];
delta = .15;
rgbSeg= sqrt ( (RGB(:,:,1)-couleur(1)).^2 + (RGB(:,:,2)-couleur(2)).^2 +(RGB(:,:,3)-couleur(3)).^2) < delta;

% close/open
rgbSeg_morph = imopen(imclose(rgbSeg,ones(3,3) ),ones(3,3) );
imshow([rgbSeg, rgbSeg_morph]);

