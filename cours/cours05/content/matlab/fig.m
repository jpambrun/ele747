addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');

%% brain mri canny
%I = im2double(rgb2gray(imread('football.jpg')));
load mri;
I= im2double(ind2gray(D(:,:,1,17), map));
Ic = edge(I,'canny',.4,1.5);
imshow([I Ic]);
ConvertPlot4Publication('brainmri_canny', 'png','on','eps','off','pdf','off','fig','off','width', 8)

%%
I = im2double((imread('cell.tif')));
%imshow(I)

sn = std2(I(100:end, 100:end))
n  = 7;

sl = stdfilt(I,ones(n,n));
mu = imfilter(I, ones(n,n)/n.^2 );

sn_over_sl = sn./sl;
sn_over_sl(sn_over_sl > 1 ) = 1;

f_hat = I - sn_over_sl .* (I - mu);

imshow([I mu; sn_over_sl f_hat])
ConvertPlot4Publication('cell_adaptative', 'png','on','eps','off','pdf','off','fig','off','width', 8)
