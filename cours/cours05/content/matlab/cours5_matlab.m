%% DoG
I= imread('cameraman.tif');
wg1=fspecial('gaussian',5,1);
wg2=fspecial('gaussian',5,1.2);
wg12=wg2-wg1;

% calcul avec le filtre combiné 
Iw12=filter2(wg12,I);

% calcul avec deux convolution
Iw1=filter2(wg1,I);
Iw2=filter2(wg2,I);

%3 seuillage à un écart type
Iwt=Iw12 > std2(Iw12);

imshow([I Iw12*40 Iwt*256; Iw1 Iw2 (Iw2-Iw1)*40]);
xlabel('a) Image originale, b) Filtree avec Iw12,  c) seuillage d) Iw1, e) Iw2 f) Iw2-Iw1')

%% Filtre adaptatif
I = im2double((imread('cell.tif')));

% paramètres
%   ici sn est estimé avec la section en bas à droite qui serait uniforme
%   sans le bruit
sn = std2(I(100:end, 100:end));
n  = 7;

% calcul de l'écart type local == sqrt(variance) et de la moyenne locale
sl = stdfilt(I,ones(n,n));
mu = imfilter(I, ones(n,n)/n.^2 );

% calcul du facteur avec le clamping a 1
% min retourne la plus petite val en 1 et sn.^2 ./ sl.^2
sn2_over_sl2 = min(sn.^2 ./ sl.^2,1);

%calcul du filtre
f_hat = I - sn2_over_sl2 .* (I - mu);

imshow([I mu; sn2_over_sl2 f_hat])
xlabel('a) I, b) I moy, c) facteur entre moy et g, d) resultat')


%% insperction d histo et bruit
I(1:80,1:80) = .45;
I(20:60,20:60)=.55;
I = imfilter(I, fspecial('gaussian',7,2), 'symmetric');
I= imnoise(I,'gaussian',0, .0001);
In= imnoise(I,'gaussian',0, .002);

subplot(2,2,1);imshow(I);
subplot(2,2,2);imshow(In);
subplot(2,2,3);imhist(I)
subplot(2,2,4);imhist(In)

%% methode iterative bloc
I(1:80,1:80) = .45;
I(20:60,20:60)=.55;
I = imfilter(I, fspecial('gaussian',7,2), 'symmetric');
I= imnoise(I,'gaussian',0, .0001);
%I= imnoise(I,'gaussian',0, .002);

T = 0.56;

% repeate 
It = I > T; 
T = ( mean( I(It) ) + mean( I(~It) ) ) / 2

imshow([I It.*I])

%% methode iterative avec saturn
I = im2double( rgb2gray( imread('saturn.png') ) );

I = I *.75+.25;  %% pour pas avoir trop de valeurs 0
I = imnoise(I,'gaussian', 0,.005);
% imhist(I);

T = 0.8;

It = I > T; 
T = ( mean( I(It) ) + mean( I(~It) ) ) / 2 %converge à 0.4597

imshow([I It.*I])

%% otsu bloc
I(1:80,1:80) = .45;
I(20:60,20:60)=.55;
I = imfilter(I, fspecial('gaussian',7,2), 'symmetric');
I= imnoise(I,'gaussian',0, .0001);
I= imnoise(I,'gaussian',0, .002);
%I=double(imread('cameraman.tif'))/255;

Ih=imhist(I);
pi=Ih/numel(I);
p1k=cumsum(pi);
mk=cumsum(pi .* (1:numel(pi))');
mtot=mk(end);
sigma_b= (mtot.*p1k-mk).^2./(p1k.*(1-p1k));


k=find(sigma_b == max(sigma_b))
kn=k/255
imshow([I I>kn]);

%% otsu saturn
I = im2double( rgb2gray( imread('saturn.png') ) );
I = I *.75+.25;  %% pour pas avoir trop de valeurs 0
I = imnoise(I,'gaussian', 0,.005);

Ih=imhist(I);
pi=Ih/numel(I);
p1k=cumsum(pi);
mk=cumsum(pi .* (1:numel(pi))');
mtot=mk(end);
sigma_b= (mtot.*p1k-mk).^2./(p1k.*(1-p1k));

maxval = max(sigma_b);
k=find(sigma_b == maxval)
kn=k/255 % 0.4627

It = I > kn; 
imshow([I It.*I])

%% point isole
I=meshgrid(1:50,1:50)/50;
I(25,25)=.55;

h=[1 1 1;1 -8 1; 1 1 1];
Ih = imfilter(I,h,'symmetric')
imshow([I, abs(Ih) * 5, abs(Ih) > 0.1 ]);

%% ligne
I=double(imread('HVDiag.png'));
wh=[-1 -1 -1; 2 2 2; -1 -1 -1];
wv=wh';
w45=[2 -1 -1;-1 2 -1; -1 -1 2];
wm45=[-1 -1 2; -1 2 -1; 2 -1 -1];
subplot(3,1,1);imshow(I);
subplot(3,1,2);imshow([abs(filter2(wh,I))/6 abs(filter2(wv,I))/6 abs(filter2(w45,I))/6 abs(filter2(wm45,I))/6])
subplot(3,1,3);imshow([abs(filter2(wh,I))/6 abs(filter2(wv,I))/6 abs(filter2(w45,I))/6 abs(filter2(wm45,I))/6]>.9)


%% compas
I=double(imread('deg_diag.png'));
Wn =[5 5 5; -3 0 -3; -3 -3 -3];
Ww = rot90(Wn);
Ws = rot90(Ww);
We = rot90(Ws);
Wnw=[5 5 -3; 5 0 -3; -3 -3 -3];
Wsw = rot90(Wnw);
Wse = rot90(Wsw);
Wne = rot90(Wse);

%1 filtre
In=imfilter(I, Wn,'replicate');
Iw=imfilter(I, Ww,'replicate');
Is=imfilter(I, Ws,'replicate');
Ie=imfilter(I, We,'replicate');

Inw=imfilter(I, Wnw,'replicate');
Isw=imfilter(I, Wsw,'replicate');
Ise=imfilter(I, Wse,'replicate');
Ine=imfilter(I, Wne,'replicate');

imshow([I In Is Ie Iw Inw Isw Ise Ine],[]);
xlabel('I, In, Is, Ie, Iw, Inw, Isw, Ise, Ine')






%% pas rapport
%% imseq
clear all;
trafficObj = VideoReader('traffic.mj2');
imcolor = read(trafficObj);
for i= 1:size(imcolor,4)
    im(:,:,i) = im2double(rgb2gray(imcolor(:,:,:,i)));
end

%1
%im1=im(:,:,71);
%im2=im(:,:,75);
%mask=abs(im1-im2)<0.1;
%imshow([im1 im2 mask abs(im1-im2) ])


%2
m=mean(im,3);
mask = abs(m - im(:,:,71))>0.05;

%3
mask1=filter2(fspecial('gaussian',5,1),  abs(m - im(:,:,71))>0.08)  ;mask=mask1;

imshow([im(:,:,71) m mask.*im(:,:,71) abs(m - im(:,:,71))])
