run('./vlfeat/toolbox/vl_setup')
%vl_version verbose
clear all
Ia = imresize(single(imread('bureau.jpg')),.33);
Ib = imresize(single(imread('wired2.jpg')),.33);
%subplot(1,2,1);imshow(I,[]);
%subplot(1,2,2);imshow(I2,[]);


[fa,da] = vl_sift(Ia) ;
[fb,db] = vl_sift(Ib) ;

[matches, scores] = vl_ubcmatch(da,db,4) ;
%figure(1) ; clf ;
figure;imshow([Ia Ib],[]) ;

xa = fa(1,matches(1,:)) ;
xb = fb(1,matches(2,:)) ;%+ size(Ia,2) ;
ya = fa(2,matches(1,:)) ;
yb = fb(2,matches(2,:)) ;

hold on ;
h = line([xa ; xb + size(Ia,2)], [ya ; yb]) ;
%set(h,'linewidth', 1, 'color', 'b') ;

vl_plotframe(fa(:,matches(1,:))) ;
fb1=fb;
fb1(1,:) = fb(1,:) + size(Ia,2) ;
vl_plotframe(fb1(:,matches(2,:))) ;
%myaa;
axis equal ;
axis off  ;


%% image transform
[sx,sy]=size(Ia);
pa=[xa;ya]';
pb=[xb;yb]';
t_proj = cp2tform(pb,pa, 'projective');
[I_t x_data y_data] = imtransform(Ib, t_proj,'XYScale',1,'FillValues',255,'XData',[1 sy],'YData',[1 sx]);
figure;imshow(I_t,[]);

P=[1,1; sy,1; sy,sx; 1,sx; 1,1];
P_t = tformfwd(t_proj,P);



figure;imshow(Ia,[]);
p=patch(P_t(:,1),P_t(:,2),'w','FaceColor','none')
set(p,'EdgeColor','g', 'LineWidth',3)


