run('./vlfeat/toolbox/vl_setup')
%vl_version verbose
clear all
Ia = imresize(single(imread('bureau.jpg')),.33);
Ib = imresize(single(imread('wired2.jpg')),.33);
%subplot(1,2,1);imshow(I,[]);
%subplot(1,2,2);imshow(I2,[]);

[fa,da] = vl_sift(Ia) ;
[fb,db] = vl_sift(Ib) ;

%% aff 50 rand
figure;imshow(Ia,[]);
perm = randperm(size(fa,2)) ; 
sel  = perm(1:50) ;
h1   = vl_plotframe(fa(:,:)) ; 
set(h1,'color','g','linewidth',1) ;
% export_fig -png frame_1
figure;imshow(Ia,[]);
h3 = vl_plotsiftdescriptor(da(:,sel),fa(:,sel)) ;  
set(h3,'color','g','linewidth',1) ;
%export_fig -png desc_1

%im2
figure;imshow(Ib,[]);
perm = randperm(size(fb,2)) ; 
sel  = perm(1:50) ;
h1   = vl_plotframe(fb(:,:)) ; 
set(h1,'color','g','linewidth',1) ;
%export_fig -png frame_2
figure;imshow(Ib,[]);
h3 = vl_plotsiftdescriptor(db(:,sel),fb(:,sel)) ;  
set(h3,'color','g','linewidth',1) ;
% export_fig -png desc_2

%% matching
[matches, scores] = vl_ubcmatch(da,db,4) ; %wired = 11, wired2
figure;imshow([Ia Ib],[]) ;

xa = fa(1,matches(1,:)) ;
xb = fb(1,matches(2,:)) ;%+ size(Ia,2) ;
ya = fa(2,matches(1,:)) ;
yb = fb(2,matches(2,:)) ;

hold on ;
h = line([xa ; xb + size(Ia,2)], [ya ; yb]) ;
%set(h,'linewidth', 1, 'color', 'b') ;

vl_plotframe(fa(:,matches(1,:)),'LineWidth',1) ;
fb1=fb;
fb1(1,:) = fb(1,:) + size(Ia,2) ;
vl_plotframe(fb1(:,matches(2,:)),'LineWidth',1) ;
%myaa;
axis equal ;
axis off  ;
% export_fig -png match


%% image transform
[sx,sy]=size(Ia);
pa=[xa;ya]';
pb=[xb;yb]';
t_proj = cp2tform(pb,pa, 'projective');
[I_t x_data y_data] = imtransform(Ib, t_proj,'XYScale',1,'FillValues',255,'XData',[1 sy],'YData',[1 sx]);
figure;imshow(I_t,[]);

P=[1,1; sy,1; sy,sx; 1,sx; 1,1];
P_t = tformfwd(t_proj,P);
% export_fig -png prj


figure;imshow(Ia,[]);
p=patch(P_t(:,1),P_t(:,2),'w','FaceColor','none')
set(p,'EdgeColor','g', 'LineWidth',3)
% export_fig -png box

