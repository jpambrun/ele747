%I=zeros(200,200);
%I(20:80,20:80)=1;
%%I(40:140,40:140)=1;
%I(120:180,120:180)=1;
clear all;
I = imread('coins.png');
figure;imshow(I)

BW = im2bw(I);
figure;imshow(BW)

BW_filled = imfill(BW,'holes');
figure;imshow(BW_filled)

boundaries = bwboundaries(BW_filled);
hold on;
for k=1:size(boundaries,1)
   b = boundaries{k};
   plot(b(:,2),b(:,1),'g','LineWidth',2);
end

%% chaincode 
cc =chaincode(b);
b = boundaries{1};
cc.code'

%% Fourrier descriptor
load spine
I = ind2gray(X, map);
%I = imread('coins.png');
BW = im2bw(I, .3);
BW_filled = imfill(BW,'holes');
boundaries = bwboundaries(BW_filled);

D=30;
b = boundaries{1};
x=b(:,2); y=b(:,1);

S = x + 1i*y;
F=fft(S);

Fs = fftshift(F);

H= zeros(size(Fs));
H(numel(Fs)/2-D:numel(Fs)/2+D)=1;
F = ifftshift(H.*Fs);
    
St=ifft(F);
xt = real(St);
yt = imag(St);

figure;
imshow([I I]);
hold on;
plot(x,y,'g','LineWidth',1)
plot(xt+490,yt,'g','LineWidth',1)

%% circ fit
% voir http://en.wikipedia.org/wiki/Linear_least_squares_(mathematics)
I = imread('coins.png');
figure;imshow(I)
BW = im2bw(I);
BW_filled = imfill(BW,'holes');
boundaries = bwboundaries(BW_filled);
b = boundaries{1};
x=b(:,2); y=b(:,1);

%linear least square avec l'equation d'un cercle
Xp  = pinv([x y ones(size(x))]); %pseudo inverse
Y = -(x.^2 + y.^2);
B = Xp*Y;

xc = -.5*B(1)
yc = -.5*B(2)
R  =  sqrt( (xc^2+yc^2) - B(3) );

figure;imshow(I)
th = linspace(0,2*pi,15)';
xe = R*cos(th)+xc; ye = R*sin(th)+yc;
hold on;
plot(xe,ye,'g','LineWidth',2)

%% Hough
clear all;
I  = imread('circuit.tif');
rotI = imrotate(I,45,'crop');

BW = edge(rotI,'canny');
[H,T,R] = hough(BW);

% Display the original image.
subplot(2,1,1);
imshow(rotI);
title('Gantrycrane Image');

% Display the Hough matrix.
subplot(2,1,2);
imshow(log(H+1),[], 'XData',T,'YData',R,'InitialMagnification','fit');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;

% lines max
P = houghpeaks(H,45,'Threshold',0.3*max(H(:)));
x = T(P(:,2));y = R(P(:,1));
plot(x,y,'s','color','r');


figure;imshow(rotI);hold on;
lines = houghlines(BW,T,R,P,'FillGap',3,'MinLength',40);
 for i = 1:numel(lines)
     p1(i,:) = lines(i).point1;
     p2(i,:) = lines(i).point2;
     xy = [lines(i).point1; lines(i).point2];
    line(xy(:,1),xy(:,2),'LineWidth',2,'Color','r');
 end


