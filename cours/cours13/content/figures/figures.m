addpath('~/Dropbox/tools/matlab/');

I = imread('coins.png');
imshow(I);removeBorder();print -dpng figure1

BW = im2bw(I);
imshow(BW);removeBorder();print -dpng figure2

BW_filled = imfill(BW,'holes');
imshow(BW_filled);removeBorder();print -dpng figure3

BW_edge = edge(BW_filled,'canny');
imshow(BW_edge);removeBorder();print -dpng figure4
