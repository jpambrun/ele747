%% ex convolution
A = [0 1 2 3 1;...
     1 2 3 3 0;...
     1 3 1 3 1;...
     1 2 2 0 3;...
     1 1 3 2 2]
 
 B = [ 0 1 0;...
      -1 0 1;...
       0 0 0]
% 1 - calculez la convolution en considérant un zéro-padding
% 2 - calculez la corrélation en considérant un padding symmétrique
   
%% sol
Ap = padarray(A,[1 1])
S1 = conv2(Ap,B, 'valid')

Ap = padarray(A,[1 1],'symmetric')
S1 = filter2(B,Ap, 'valid')

%% ex histogramme
A = [0 1 2 3 4 5;...
     6 7 5 4 2 1;...
     1 3 2 4 1 0;...
     1 2 4 2 3 1;...
     3 2 3 5 1 3;...
     1 1 4 2 2 2]
% 1 - Calculez l'histogramme
% 2 - Égalisez l'histogramme 
 
%% solution
n  = histc(A(:),0:7)
pr = n/(6*6)
% Attention : les indices commencent à 1 dans matlab.
% C'est pourquoi pr(0:k) commence plutôt à un.
sk_0 = (2^3-1) .* sum( pr(1:1) );
sk_1 = (2^3-1) .* sum( pr(1:2) );
sk_2 = (2^3-1) .* sum( pr(1:3) );
sk_3 = (2^3-1) .* sum( pr(1:4) );
sk_4 = (2^3-1) .* sum( pr(1:5) );
sk_5 = (2^3-1) .* sum( pr(1:6) );
sk_6 = (2^3-1) .* sum( pr(1:7) );
sk_7 = (2^3-1) .* sum( pr(1:8) );
sk = [sk_0;sk_1;sk_2;sk_3;sk_4;sk_5;sk_6;sk_7]
sk = round(sk)