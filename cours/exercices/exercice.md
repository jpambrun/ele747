Chapitre 2 - ensemble, voisinage, chemin
=================
2.11
2.15
2.16 (Fait la différence entre chemin et distance)
2.23 a et b

Chapitre 3 - Histogramme, transformation d'intensité
=================
3.1
3.5a ensemble
3.13a ensemble, seulement quand f ou g est canstant. L'histogramme contient
			aucun info spatial.
3.14 considérant un 0-padding, taille NxN, calculer l'histo de gauche
3.21 ensemble
3.23

> Exemple de convolution
> Exemple d'égalisation d'histo matlab

Chapitre 10- Seg, Thresholding
=================
10.2
10.6 30sec
10.7
10.10 ensemble, deja fait en classe


Chapitre 9 - Morpho
=================
9.7 ensemble
9.8 .bof
9.17
9.19 ensemble
9.36

Chapitre 6 - Couleur
=================


Chapitre 4 - Fourier
=================
4.12
4.13
4.15
4.21
4.22
  4.23?
  Exemple numérique de F(u, v)


Chapitre 8 - Image compression
=================
8.1  ensemble
8.3  a) b) PSNR au lieu de SNR
8.5
8.9  abc donner réponse pour def
8.10 copier Fig8.8 symbole / Code avant
8.17
8.18
8.19


