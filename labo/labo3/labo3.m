%% ##########################################################################
%%                      ÉTUDIANTS : copy == zéro !!
%% ##########################################################################

%% init
addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');
clear all;
close all;

%fig 1
I =imread('ressources/dollard1.tif');
imshow(I)
ConvertPlot4Publication('figure1', 'png','on','eps','off','pdf','off','fig','off','width', 3*2)
close all;

%% fig 2
I  = im2double(imread('ressources/dollard1.tif'));
I  = double(I>.5);
In = imnoise(I,'salt & pepper');

I  = imresize(I,4,'nearest');
In  = imresize(In,4,'nearest');

imshow(In)
ConvertPlot4Publication('figure2a', 'png','on','eps','off','pdf','off','fig','off','width', 3*2);close all;
imshow(I)
ConvertPlot4Publication('figure2b', 'png','on','eps','off','pdf','off','fig','off','width', 3*2,'backcolor',[.99 .99 .99] );close all;


%% fig 3
I  = im2double(imread('ressources/formes.tif'));
I  = imresize(I,6,'nearest');
imshow(I)
ConvertPlot4Publication('figure3', 'png','on','eps','off','pdf','off','fig','off','width', 3*2);close all;