%% ##########################################################################
%%                      ÉTUDIANTS : copy == zéro !!
%% ##########################################################################

%% init
addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');
clear all;
close all;


%% fig1
I=im2double(imread('/home/jpambrun/Downloads/1405774_46782888.jpg'));
I = imresize(I,.33);
I_hsi = rgb2hsv(I);
imshow(I_hsi(:,:,1),[])
%BW = I_hsi(:,:,1)<.19 & I_hsi(:,:,1)>.14; %vert
%%
BW1 = I_hsi(:,:,1)<.002 | I_hsi(:,:,1)>.9; %rouge2
Ih(:,:,1) = BW1.*I(:,:,1); Is(:,:,2) = BW1.*I(:,:,2);Is(:,:,3) = BW1.*I(:,:,3);
close all;
imshow(Ih)

%%
BW2 = I_hsi(:,:,2)>.1; %rouge3
BW = BW1&BW2;

Is(:,:,1) = BW.*I(:,:,1); Is(:,:,2) = BW.*I(:,:,2);Is(:,:,3) = BW.*I(:,:,3);



close all;
imshow(Is)

