%% ##########################################################################
%%                      ÉTUDIANTS : copy == zéro !!
%% ##########################################################################

%% init
addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');
clear all;
close all;

%% fig 1
load spine; I = ind2gray(X,map);

figure;imshow(I);
ConvertPlot4Publication('figure1a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5)
close all;

figure;imshow(abs(filter2(fspecial('sobel'),I))+abs(filter2(fspecial('sobel')',I)),[]);
ConvertPlot4Publication('figure1b', 'png','on','eps','off','pdf','off','fig','off','width', 2.5)
close all;

%% fig
close all;
load spine; I = ind2gray(X,map);

figure;imshow(I);
line([170 315], [222 222],'LineWidth',2,'Color',[1 1 1]);
text(170-15,222-12,'A','Color',[1 1 1],'FontSize',16,'Interpreter','latex');
text(315,222-12,'B','Color',[1 1 1],'FontSize',16,'Interpreter','latex');
ConvertPlot4Publication('figure2', 'png','on','eps','off','pdf','off','fig','off','width', 3)
close all;
