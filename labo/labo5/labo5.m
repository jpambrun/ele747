%% ##########################################################################
%%                      ÉTUDIANTS : copy == zéro !!
%% ##########################################################################

%% init
addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');
clear all;
close all;

%%
I = im2double(rgb2gray(imread('saturn.png')));
F = fftshift(fft2(I));

imshow(log10(abs(F)+1),[])
ConvertPlot4Publication('figure1b', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)
close all;

imshow(I)
ConvertPlot4Publication('figure1a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)
close all;


%%
clear all;
I = zeros(300,300);
I(150,100)=1;I(150,200)=1;
I=bwdist(I);
I=I<=4;
I=~I;
imshow(I,[]);

Ib=I
Ib(1,:)=0;Ib(:,1)=0;Ib(:,300)=0;Ib(300,:)=0;
imwrite(Ib, 'figure2a.png');
close all;

G= zeros(300,300);
G(150,150)=1;
G=bwdist(G);
G=G<=20;

F = fftshift(fft2(I));
F = F.* G;
If = ifft2(ifftshift(F));
imshow(abs(If),[])
Ib=If;
Ib(1,:)=0;Ib(:,1)=0;Ib(:,300)=0;Ib(300,:)=0;
imwrite(Ib, 'figure2b.png');
close all;


%%
clear all;
h=1/9.*[-1 -1 -1; -1 8 -1;-1 -1 -1];
F = fftshift(fft2(h, 400,400*4/3));
imshow(log10(abs(F)+1),[]);
colormap(jet);
ConvertPlot4Publication('figure3', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)



