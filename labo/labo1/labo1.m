
%% ##########################################################################
%%                      ÉTUDIANTS : copy == zéro !!
%% ##########################################################################

%% init
addpath('/home/jpambrun/Dropbox/tools/matlab/ConvertPlot4Publication')
set(0, 'DefaultFigureRenderer', 'painters');

%% figure 1
clear all;
close all;
set(gcf, 'Renderer', 'painters')
load clown; I = ind2gray(X,map);
n = hist(I(:),128);

figure;imshow(I);colormap(gray(128))
ConvertPlot4Publication('figure1a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)


figure;plot(n/1000,'k.','MarkerSize',7);axis([0 128 0 8.5])
xlabel('Classe de l''histogramme','Interpreter','Latex')
ylabel('Nombre d''occurences','Interpreter','Latex')
ConvertPlot4Publication('figure1b' ,'fig','off','png','off','eps','off', 'linewidth',0.2,'width', 2.5)

%% figure 2
clear all;
close all;

load spine; I = ind2gray(X,map);

figure;imshow(I);colormap(gray(64))
line([32 387],[36 154],'Color','w','LineWidth',2)
ConvertPlot4Publication('figure2a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)

c =improfile(I,[32 387],[36 154]);
figure;plot(c, 'k');
xlabel('Distance le long du profil','Interpreter','Latex')
ylabel('Intensit\''e','Interpreter','Latex')
ConvertPlot4Publication('figure2b' ,'fig','off','png','off','eps','off', 'linewidth',0.2,'width', 2.5)


%% figure 3
load trees;
I = ind2gray(X,map);
BW=(I>0.5);
figure;imshow(I)
ConvertPlot4Publication('figure3a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)
figure;imshow(BW)
ConvertPlot4Publication('figure3b', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)


%% figure 4
load clown; I = ind2gray(X,map);
imshow(I)
ConvertPlot4Publication('figure4a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)

imshow(filter2(fspecial('average',5),I));
ConvertPlot4Publication('figure4b', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)

%% figure 5
I = rgb2gray(imread('autumn.tif'));
In= imnoise(I,'salt & pepper');
imshow(In)
ConvertPlot4Publication('figure5a', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)

imshow(medfilt2(In,[3 3]));
ConvertPlot4Publication('figure5b', 'png','on','eps','off','pdf','off','fig','off','width', 2.5*2)

